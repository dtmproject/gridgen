/**
 * @file main.cc
 * @author Uwe Koecher (UK)
 * @date 2015-02-10, UK
 *
 * @brief DTM++/GridGen: Grid Generator for deal.II.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/GridGen                                        */
/*                                                                            */
/*  DTM++/GridGen is free software: you can redistribute it and/or modify     */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/GridGen is distributed in the hope that it will be useful,          */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/GridGen.   If not, see <http://www.gnu.org/licenses/>.   */


// PROJECT includes

// DEAL.II includes
#include <deal.II/base/exceptions.h>
#include <deal.II/base/logstream.h>
#include <deal.II/base/utilities.h>

// GridGenerator includes
#include <deal.II/base/point.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/grid_in.h>
#include <deal.II/grid/grid_out.h>
#include <deal.II/grid/tria.h>

#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/dofs/dof_handler.h>
#include <deal.II/fe/fe.h>
#include <deal.II/fe/fe_q.h>
#include <deal.II/fe/mapping.h>
#include <deal.II/fe/mapping_q.h>
#include <deal.II/fe/fe_values.h>
#include <deal.II/lac/vector.h>
#include <deal.II/base/quadrature.h>
#include <deal.II/base/quadrature_lib.h>

#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/data_component_interpretation.h>


#ifndef dim
#define dim 2
#endif

#if GridGen_USE_SHARED_LIBS==1
#  include <dlfcn.h>
#  ifdef GridGen_HAVE_LINK_H
#    include <link.h>
#  endif
#endif

// C++ includes
#include <iostream>
#include <fstream>
#include <memory>
#include <vector>
#include <string>

int main(int argc, char *argv[]) {
	// Create individual output file for each running process
	const unsigned int MyPID(0);
	std::ostringstream filename;
	filename << "proc" << MyPID << ".log";
	std::ofstream pout(filename.str().c_str());
	
	try {
		////////////////////////////////////////////////////////////////////////
		// Init application
		//
		
		// Attach deallog to process output
		dealii::deallog.attach(pout);
		dealii::deallog.depth_console(0);
		pout	<< "****************************************"
				<< "****************************************"
				<< std::endl;
		
		//
		////////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////////////
		// Begin application
		//
		
		// Create mesh with the dealii::GridGenerator
		// ...
		
		dealii::Triangulation<dim> tria;
		
		{
			dealii::GridIn<dim> grid_in;
			grid_in.attach_triangulation(tria);
			std::ifstream input_file("slide.inp");
			grid_in.read_ucd(input_file);
		}
		
		
		// minimal fe/dof
		dealii::FE_Q<dim> fe(1);
		dealii::DoFHandler<dim> dof(tria);
		dof.distribute_dofs(fe);
		
		dealii::MappingQ<dim> mapping(1);
		dealii::QGaussLobatto<dim-1> face_quad(2);
		dealii::FEFaceValues<dim> fe_face_values(
			mapping,
			fe,
			face_quad,
			dealii::update_quadrature_points
		);
		
		const dealii::types::global_dof_index n_dofs(dof.n_dofs());
		const unsigned int dofs_per_cell(dof.get_fe().dofs_per_cell);
		
		std::vector<dealii::types::global_dof_index> dofs(dofs_per_cell);
		
		dealii::Vector<double> u(n_dofs);
		dealii::Vector<double> u_local(dofs_per_cell);
		
		
		// Colorize boundaries
		// ...
		
// 		auto cell = tria.begin_active();
// 		auto endc = tria.end();
		
		auto cell = dof.begin_active();
		auto endc = dof.end();
		
		u = 0;
		
		for ( ; cell != endc; ++cell ) {
			cell->get_dof_indices(dofs);
			u_local = 0;
			
			for (unsigned int face(0); face < dealii::GeometryInfo<dim>::faces_per_cell; ++face) {
				// reset boundary_indicator
				if (cell->face(face)->at_boundary()) {
					switch (cell->face(face)->boundary_indicator()) {
					case 1:
						cell->face(face)->set_boundary_indicator(5);
						break;
					
					case 2:
						cell->face(face)->set_boundary_indicator(4);
						break;
					
					case 3:
						cell->face(face)->set_boundary_indicator(5);
						break;
					
					case 4:
						cell->face(face)->set_boundary_indicator(5);
						break;
					
					default:
						AssertThrow(false, dealii::ExcNotImplemented());
					}
				}
				
				// evaluate boundary_id into solution vector
				if (cell->face(face)->at_boundary()) {
					fe_face_values.reinit(cell,face);
					
					for (unsigned int i(0); i < dofs_per_cell; ++i) {
						if (fe_face_values.get_fe().has_support_on_face(i,face)) {
							u_local[i] = cell->face(face)->boundary_indicator();
						}
					}
					
					for (unsigned int i(0); i < dofs_per_cell; ++i) {
						u[dofs[i]] = u_local[i];
					}
				}
			}
		}
		
		// write out solution containing boundary_id's
		{
			dealii::DataOut<dim> data_out;
			data_out.attach_dof_handler(dof);
			
			std::vector<std::string> component_names;
			component_names.push_back("boundary_id");
			
			std::vector<
				dealii::DataComponentInterpretation::DataComponentInterpretation
			> dci;
			dci.push_back(dealii::DataComponentInterpretation::component_is_scalar);
			
			data_out.add_data_vector(
				u,
				component_names,
				dealii::DataOut<dim>::type_dof_data,
				dci
			);
			
			data_out.build_patches(1);
			
			std::string filename = "boundary_id.vtk";
			std::ofstream output(filename.c_str());
			
			data_out.write_vtk(output);
		}
		
		// Write out the mesh description
		// ...
		
		std::ostringstream filename;
		dealii::GridOut grid_out;
		
// 		// MSH format
// 		filename << "grid.msh";
// 		std::ofstream gout(filename.str().c_str());
// 		grid_out.write_msh(tria, gout);
		
		// UCD format
		filename << "grid.ucd";
		std::ofstream gout(filename.str().c_str());
		dealii::GridOutFlags::Ucd ucd_flags;
		ucd_flags.write_lines = true;
		ucd_flags.write_faces = true;
		
		grid_out.set_flags(ucd_flags);
		grid_out.write_ucd(tria, gout);
		
		gout.close();
		
		pout << std::endl << "Goodbye." << std::endl;
		
		//
		// End application
		////////////////////////////////////////////////////////////////////////
	}
	catch (std::exception &exc) {
		std::cerr	<< std::endl
					<< "****************************************"
					<< "****************************************"
					<< std::endl << std::endl
					<< "An EXCEPTION occured: Please READ the following output CAREFULLY!"
					<< std::endl;
		
		std::cerr	<< exc.what() << std::endl;
		
		std::cerr	<< std::endl
					<< "APPLICATION TERMINATED unexpectedly due to an exception."
					<< std::endl << std::endl
					<< "****************************************"
					<< "****************************************"
					<< std::endl << std::endl;
		
		// LOG error message to individual process output file.
		pout	<< std::endl
				<< "****************************************"
				<< "****************************************"
				<< std::endl << std::endl
				<< "An EXCEPTION occured: Please READ the following output CAREFULLY!"
				<< std::endl;
		
		pout	<< exc.what() << std::endl;
		
		pout	<< std::endl
				<< "APPLICATION TERMINATED unexpectedly due to an exception."
				<< std::endl << std::endl
				<< "****************************************"
				<< "****************************************"
				<< std::endl << std::endl;
		
		// Close output file stream
		pout.close();
		
		return 1;
	}
	catch (...) {
		std::cerr	<< std::endl
					<< "****************************************"
					<< "****************************************"
					<< std::endl << std::endl
					<< "An UNKNOWN EXCEPTION occured!"
					<< std::endl;
		
		std::cerr	<< std::endl
					<< "----------------------------------------"
					<< "----------------------------------------"
					<< std::endl << std::endl
					<< "Further information:" << std::endl
					<< "\tThe main() function catched an exception"
					<< std::endl
					<< "\twhich is not inherited from std::exception."
					<< std::endl
					<< "\tYou have probably called 'throw' somewhere,"
					<< std::endl
					<< "\tif you do not have done this, please contact the authors!"
					<< std::endl << std::endl
					<< "----------------------------------------"
					<< "----------------------------------------"
					<< std::endl;
		
		std::cerr	<< std::endl
					<< "APPLICATION TERMINATED unexpectedly due to an exception."
					<< std::endl << std::endl
					<< "****************************************"
					<< "****************************************"
					<< std::endl << std::endl;
		
		// LOG error message to individual process output file.
		pout	<< std::endl
				<< "****************************************"
				<< "****************************************"
				<< std::endl << std::endl
				<< "An UNKNOWN EXCEPTION occured!"
				<< std::endl;
		
		pout	<< std::endl
				<< "----------------------------------------"
				<< "----------------------------------------"
				<< std::endl << std::endl
				<< "Further information:" << std::endl
				<< "\tThe main() function catched an exception"
				<< std::endl
				<< "\twhich is not inherited from std::exception."
				<< std::endl
				<< "\tYou have probably called 'throw' somewhere,"
				<< std::endl
				<< "\tif you do not have done this, please contact the authors!"
				<< std::endl << std::endl
				<< "----------------------------------------"
				<< "----------------------------------------"
				<< std::endl;
		
		pout	<< std::endl
				<< "APPLICATION TERMINATED unexpectedly due to an exception."
				<< std::endl << std::endl
				<< "****************************************"
				<< "****************************************"
				<< std::endl << std::endl;
		
		// Close output file stream
		pout.close();
		
		return 1;
	}
	
	// Close output file stream
	pout.close();
	
	return 0;
}
